import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DiscountRecordsService } from './services/discount_records.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { WelcomeComponent } from './components/welcome/welcome.component';

import { LoadingComponent } from './components/loading/loading.component';

import { AlertComponent } from './components/alert/alert.component';
import { ConfigurationService } from './services/configuration.service';
import { CategoryComponent } from './components/category/category.component';
import { DiscountRecordComponent } from './components/discount-record/discount-record.component';
import { DiscountRecordsComponent } from './components/discount-records/discount-records.component';
import { HomeComponent } from './components/home/home.component';

const appRoutes: Routes = [
    {
        path: '',
        component: WelcomeComponent,
        children: [
            { path: '', component: HomeComponent },
            { path: 'category/:category', component: CategoryComponent }

        ]
    },
];

@NgModule({
    declarations: [
        AppComponent,
        SideBarComponent,
        NavBarComponent,
        WelcomeComponent,
        LoadingComponent,
        AlertComponent,
        CategoryComponent,
        DiscountRecordComponent,
        DiscountRecordsComponent,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        MDBBootstrapModule.forRoot(),
        RouterModule.forRoot(
            appRoutes
        )
    ],
    providers: [
        DiscountRecordsService,
        ConfigurationService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
