export interface IDiscountRecord {
    id: number;
    name: string;
    description: string;
    active: string;
    created_at: Date;
    update_at: Date;

}

export interface IDiscountRecords {
    data: IDiscountRecords[];
    results: number;
}