import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICategories } from '../interfaces/Categories';
import { IDiscountRecords } from '../interfaces/DiscountRecord';
import { Observable } from 'rxjs';
import { ConfigurationService } from './configuration.service';

@Injectable({
    providedIn: 'root'
})
export class DiscountRecordsService {

    constructor(private http: HttpClient, private Config: ConfigurationService) { }

    getDiscountRecord(recordId): Observable<IDiscountRecords> {
        let url = this.Config.getConfig('apps.api');
        url = url + 'v1/discount-record/' + recordId;
        if (url != null) {
            return this.http.get<IDiscountRecords>(url);
        } else {
            return null;
        }
    }

    getDiscountRecords(lat, lng, conditions): Observable<IDiscountRecords> {
        let url = this.Config.getConfig('apps.api');
        url = url + 'v1/discount-records/ ' + lat + ' / ' + lng;


        if (conditions != null) {
            let conditionStrings = [];
            for (var key in conditions) {
                conditionStrings.push(key + '=' + conditions[key]);
            }
            if (conditionStrings.length > 0) {
                url = url + '?' + conditionStrings.join("&");
            }
        }

        console.log(encodeURI(url));

        if (url != null) {
            return this.http.get<IDiscountRecords>(url);
        } else {
            return null;
        }
    }

    getCategories(lat = null, lng = null): Observable<ICategories> {
        let url = this.Config.getConfig('apps.api');
        url = url + 'v1/categories/';
        if (lat != null && lng != null) {
            url = url + lat + "/" + lng;
        }
        if (url != null) {
            return this.http.get<ICategories>(url);
        } else {
            return null;
        }
    }
}
