import { Injectable } from '@angular/core';
import { config } from '../config/app';

// Interfaces here

@Injectable()
export class ConfigurationService {
    private token: string;
    private expiry: string;

    constructor() { }

    public getConfig(dir, def = null) {
        let dirSplit = dir.split('.');
        let current = config;

        for (let i in dirSplit) {
            let val = dirSplit[i];
            if (current[val] == null) {
                return null;
            } else {
                current = current[val];
            }
        }

        return current.toString();
    }
}