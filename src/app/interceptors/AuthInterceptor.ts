import { HttpHandler, HttpInterceptor, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ConfigurationService } from '../services/configuration.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private Config: ConfigurationService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url = this.Config.getConfig('apps.personaApi');
        if (req.url.startsWith(url)) {
            const token = localStorage.getItem('auth-token');
            if (token != null) {
                const cloned = req.clone({
                    headers: req.headers.set('auth', token)
                });
                return next.handle(cloned);
            }

        }
        return next.handle(req);
    }
}