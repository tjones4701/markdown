import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

    @Input() type;
    @Input() title;
    class = '';
    icon = '';
    constructor() { }

    ngOnInit() {
        switch (this.type) {
            case 'danger':
            case 'error':
                this.class = 'bg-danger';
                this.icon = 'fa-times-circle-o';
                break;
            case 'success':
                this.icon = 'fa-check';
                this.class = 'bg-success';
                break;
            case 'warning':
                this.icon = 'fa-warning';
                this.class = 'bg-warning';
                break;
            default:
                this.icon = 'fa-question-circle-o';
                this.class = 'bg-info';
        }

        this.class = this.class + ' card z-depth-2 w-100 h-100 p-2 persona-alert';


    }

}
