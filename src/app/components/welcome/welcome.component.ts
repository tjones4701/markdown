import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscountRecordsService } from '../../services/discount_records.service';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

    public navExpanded = false;
    public categories: string[] = null;
    public loaded = false;
    public navGroups = [];

    constructor(private _discountRecordsOService: DiscountRecordsService, private router: Router, private route: ActivatedRoute) { }

    ngOnInit() {
        this.navGroups = [];
        navigator.geolocation.getCurrentPosition((position) => {
            let lat = position.coords.latitude;
            let lng = position.coords.longitude;
            let state = this._discountRecordsOService.getCategories(lat, lng).subscribe((data) => {
                if (data != null && data.data != null) {
                    this.categories = data.data;

                    var categoriesGroup = {
                        title: 'Categories',
                        icon: 'fa-users',
                        children: {}
                    };

                    this.navGroups = [{
                        title: 'Our Recommendations',
                        icon: 'fa-users',
                        children: {
                            'Cafes': 'category/cafes',
                            'Accommodations': 'category/accommodation',
                            'Clothing': 'category/clothing',
                            'Travel': 'category/Travel%20Agencies'
                        }
                    }];

                    for (var categoryKey in this.categories) {
                        var cat = this.categories[categoryKey];
                        categoriesGroup.children[cat] = 'category/' + cat;
                    }

                    this.navGroups.push(categoriesGroup);

                    this.loaded = true;
                } else {
                    this.router.navigate(['error/organisation-view/unable to load organisation']);
                }
            }, (error) => {
                console.log(error);
            });
            this.navGroups = [{
                title: 'Loading',
                icon: 'fa-loading',
            }];
        });
    }

    public toggleNav() {
        this.navExpanded = !this.navExpanded;
    }

    public setNav(status) {
        this.navExpanded = status;
    }

    onNavigate(route) {
        if (window.outerWidth < 768) {
            this.setNav(false);
        }
    }

}
