import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'app-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

    constructor(private router: Router) { }

    @Input() navGroups;
    @Output() onNavigate = new EventEmitter();
    children = {};

    updateChildren() {
        for (const key in this.navGroups) {
            let navGroup = this.navGroups[key];
            if (navGroup.title != null) {

                if (this.children[navGroup.title] == null) {
                    this.children[navGroup.title] = [];
                }

                if (navGroup.children != null) {
                    for (const childKey in navGroup.children) {
                        var route = navGroup.children[childKey];

                        var item = {
                            title: childKey,
                            route: route
                        };
                        this.children[navGroup.title].push(item);
                    }
                }
            }
        }
    }

    ngOnInit() {
        this.updateChildren();
    }

    ngOnChanges(changes) {
        this.updateChildren();
    }

    navigate(event, url) {
        event.stopPropagation();
        this.onNavigate.emit(url);
        this.router.navigate([url]);
    }

}
