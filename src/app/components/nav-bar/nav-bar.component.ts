import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
    @Input() title: string;
    @Input() loading: boolean;
    @Input() type: string = "base";
    @HostBinding('class.nav-bar-base') type_base: boolean = false;
    @HostBinding('class.nav-bar') type_organisation: boolean = false;
    private loggedIn = false;

    constructor() { }



    ngOnInit() {
        if (this.type != null && this["type_" + this.type] != null) {
            this["type_" + this.type] = true;
        }


    }

    public changeLogout(state) {
        this.loggedIn = state;
    }


}
