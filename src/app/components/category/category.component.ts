import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

    constructor(private route: ActivatedRoute, private router: Router) { }
    @Input() category: string;

    ngOnInit() {
        var self = this;
        if (this.category == null) {
            this.router.events.subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    self.category = self.route.snapshot.paramMap.get('category');
                }
            });
        };
    }

}
