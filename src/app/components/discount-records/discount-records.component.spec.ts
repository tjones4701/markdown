import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountRecordsComponent } from './discount-records.component';

describe('DiscountRecordsComponent', () => {
  let component: DiscountRecordsComponent;
  let fixture: ComponentFixture<DiscountRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
