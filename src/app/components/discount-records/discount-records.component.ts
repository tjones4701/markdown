import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DiscountRecordsService } from '../../services/discount_records.service';


@Component({
    selector: 'app-discount-records',
    templateUrl: './discount-records.component.html',
    styleUrls: ['./discount-records.component.scss']
})
export class DiscountRecordsComponent implements OnInit {

    loading = false;
    constructor(private _discountRecordsOService: DiscountRecordsService, private router: Router, private route: ActivatedRoute) { }


    @Input() category: string;
    discountRecords = [];
    loaded = false;

    ngOnChanges(changes) {
        this.loadRecords();
    }

    loadRecords() {
        var self = this;
        this.loaded = false;
        navigator.geolocation.getCurrentPosition((position) => {
            let lat = position.coords.latitude;
            let lng = position.coords.longitude;

            var conditions = {
                limit: 50,
                'max-distance': 50
            };

            if (this.category == null) {
                this.category = self.route.snapshot.paramMap.get('category');
            }

            if (this.category != null) {
                conditions['category'] = this.category.toLowerCase();
            }

            let state = this._discountRecordsOService.getDiscountRecords(lat, lng, conditions).subscribe((data) => {
                if (data != null && data.data != null) {
                    this.discountRecords = data.data;

                    this.loaded = true;
                } else {
                    this.router.navigate(['error/organisation-view/unable to load organisation']);
                }
            }, (error) => {
                console.log(error);
            });
        });
    }

    ngOnInit() {
        this.loadRecords();
    }

}
