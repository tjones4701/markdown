import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountRecordComponent } from './discount-record.component';

describe('DiscountRecordComponent', () => {
  let component: DiscountRecordComponent;
  let fixture: ComponentFixture<DiscountRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
