import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-discount-record',
    templateUrl: './discount-record.component.html',
    styleUrls: ['./discount-record.component.scss']
})
export class DiscountRecordComponent implements OnInit {

    @Input() discountRecord;
    constructor() { }

    ngOnInit() {
    }

}
